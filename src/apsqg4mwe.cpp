/**
 * @file
 * @brief Executable for the Geant4 Minimal Working Example code of Allpix Squared
 *
 * @copyright Copyright (c) 2021 CERN and the Allpix Squared authors.
 * This software is distributed under the terms of the MIT License, copied verbatim in the file "LICENSE.md".
 * In applying this license, CERN does not waive the privileges and immunities granted to it by virtue of its status as an
 * Intergovernmental Organization or submit itself to any jurisdiction.
 */

#include <atomic>
#include <csignal>
#include <cstdlib>
#include <fstream>
#include <memory>
#include <string>
#include <utility>

#include <boost/version.hpp>
#include <G4Version.hh>

#include "geometry/GeometryBuilderGeant4Module.hpp"

using namespace allpix;

/**
 * @brief Main function running the application
 */
int main(int argc, const char* argv[]) {

    int return_code = 0;
    try {
        // Construct main Allpix object
        auto geometrybuilder = std::make_unique<GeometryBuilderGeant4Module>();

        // Initialize geometry
        geometrybuilder->initialize();

        // Run modules and event-loop
        // apx->run();

        // Finalize modules (post-run)
        // apx->finalize();
    } catch(std::exception& e) {
        std::cout << "Fatal internal error" << std::endl << e.what() << std::endl << "Cannot continue." << std::endl;
        return_code = 127;
    }

    return return_code;
}
